# Noelia Arroyo, Práctica3_Ejercicio4_Ejercicio5
# !/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler
from smallsmilhandler import SmallSMILHandler
import json
import sys


def lista(list):
    for data in list:
        cadena = data['etiqueta']
        for atributo in data:
            cadena += atributo + "=\"" + data[atributo] + "\"\t"
        print(cadena)


def json(self, fichero, fich_json):
    fich_json = fichero.replace(".smil", ".json")

    with open(fich_json, 'w') as archivo:
        json.dump(list, archivo)


if __name__ == "__main__":
    """
    Programa principal
    """
    if len(sys.argv) != 2:
        sys.exit("Usage: python3 karaoke.py file.smil")
    fichero = sys.argv[1]

parser = make_parser()
cHandler = SmallSMILHandler()
parser.setContentHandler(cHandler)
parser.parse(open(fichero))
lista(cHandler.get_tags())
json(cHandler.get_tags())
