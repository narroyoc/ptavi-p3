# Noelia Arroyo, Práctica3_Ejercicio3
# !/usr/bin/python3
# -*- coding: utf-8 -*-

from xml.sax import make_parser
from xml.sax.handler import ContentHandler


class SmallSMILHandler(ContentHandler):
    def __init__(self):
        self.list = []
        self.labels = {
            "root-layout": ["background-color", "height", "width"],
            "region": ["bottom", "id", "top", "left", "right"],
            "img": ["src", "region", "begin", "dur"],
            "audio":  ["src", "begin", "dur"],
            "textstream": ["src", "region"]
        }

    def startElement(self, name, attrs):
        """
        Método que se llama cuando se abre una etiqueta
        """

        if name in self.labels:
            marca = {'etiqueta': name}
            for atributo in self.labels[name]:
                marca[atributo] = attrs.get(atributo, "")
            self.list.append(marca)

    def get_tags(self):
        return self.list


if __name__ == "__main__":
    """
    Programa principal
    """
    parser = make_parser()
    cHandler = SmallSMILHandler()
    parser.setContentHandler(cHandler)
    parser.parse(open('karaoke.smil'))
    list = cHandler.get_tags()
    print(list)
